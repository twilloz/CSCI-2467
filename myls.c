/** homework1
  * Author: Tracey Willoz
  * CSCI-2467 -C Practice
  * Date: Feburary 3,2016
  */

#include "apue.h"
#include <dirent.h>

int main(int argc, char *argv[])
{
	DIR			*dp;
	struct dirent		*dirp;
	if (argc ==1){
		dp = opendir(".");
		if ( dp == NULL ){
			err_sys("can't open %s",argv[1]);
		}
		while ( (dirp = readdir(dp) ) != NULL ){
			if (strncmp(dirp->d_name, ".", 1) != 0){
				printf("%s%s", dirp->d_name, "   ");
			}
		}
	}

	else if(argc == 2){
		if(strcmp(argv[1], "-a") == 0){
			if ( (dp = opendir("."))  == NULL ){
				err_sys("can't open %s",argv[1]);
			}
			while ( (dirp = readdir(dp) ) != NULL ){
				if (strncmp(dirp->d_name, ".", 1) == 0){
					printf("%s%s", dirp->d_name, "   ");
				}
			}
		}
		else{
			if ( (dp = opendir(argv[1]))  == NULL )
				err_sys("can't open %s",argv[1]);
			while ( (dirp = readdir(dp) ) != NULL ){
				if (strncmp(dirp->d_name, ".", 1) != 0){
					printf("%s%s", dirp->d_name, "   ");
				}
			}

		}	
	}

	else if(argc == 3){
		if ( (dp = opendir(argv[1]))  == NULL )
			err_sys("can't open %s",argv[1]);
		if((strcmp(argv[2], "-a") == 0)){
			while ( (dirp = readdir(dp) ) != NULL ){
				if (strncmp(dirp->d_name, ".", 1) == 0){
					printf("%s%s", dirp->d_name, "   ");
				}
			}
		}
		else{
			printf("Invalid Flag...\n");
			exit(1);
		}

	}

	else
		err_quit("usage:  myls directory_name");

	
	printf("\n");
	closedir(dp);
	exit(0);
}
