/** Homework 3
  * Author: Tracey Willoz
  * Create processes; put them to sleep; print ids
  * Date: April 27,2016
  */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

void processId(void);

int main(int argc, char *argv[]) {
	pid_t parentId;
	pid_t childId;
	pid_t gchildId;

	if (argc < 2 || argc > 2) {
		printf("Invalid Argument...");
	}

	else{
		if(*argv[1] == '1'){
			printf("\nThe Grandchild process finishes first. \n");
			printf("The Child process finishes second. \n");
			printf("The Parent process finishes last. \n");
			
			//create parent
			if((parentId = fork()) < 0){
				printf("Fork Error...\n");
			}
			
			else if(parentId == 0){
				printf("\nNew Processes...");
				printf("\n     PARENT PROCESS\n");
				processId();

				//create child
				if((childId = fork()) < 0){
					printf("Fork Error...\n");
				}
			
				//inside child	
				else if(childId == 0){
					printf("\n     CHILD PROCESS\n");
					processId();

					//create grandchild	
					if((gchildId = fork()) < 0){
							printf("Fork Error...\n");
					}
				
					else if(gchildId == 0){
						printf("\n     GRANDCHILD PROCESS\n");
						processId();
						sleep(2);
						printf("\nAfter Sleeping...");
						printf("\n     GRANDCHILD PROCESS\n");
						processId();
						exit(0);
					}//leaving grandchild process
				
					sleep(5);
					printf("\n     CHILD PROCESS\n");
					processId();
					exit(0);
				}//leaving child
			
				sleep(7);
				printf("\n     PARENT PROCESS\n");
				processId();
				exit(0);
			}//leaving parent	
		}//end if 1
		
		else if(*argv[1] == '2'){
			printf("\nThe Child process finishes first. \n");
			printf("The Parent process finishes second. \n");
			printf("The GrandChild process finishes last. \n");
			
			//inside the parent
			if((parentId = fork()) < 0){
				printf("Fork Error...\n");
			}
			
			else if(parentId == 0){
				printf("\nNew Processes...");
				printf("\n     PARENT PROCESS\n");
				processId();

				//create child
				if((childId = fork()) < 0){
					printf("Fork Error...\n");
				}
			
				//inside child	
				else if(childId == 0){
					printf("\n     CHILD PROCESS\n");
					processId();
			
					//create grandchild	
					if((gchildId = fork()) < 0){
							printf("Fork Error...\n");
					}
				
					else if(gchildId == 0){
						printf("\n     GRANDCHILD PROCESS\n");
						processId();
						sleep(7);
						printf("\n     GRANDCHILD PROCESS\n");
						processId();
						exit(0);	
					}//leaving grandchild process
				
					sleep(2);
					printf("\nAfter Sleeping...");
					printf("\n     CHILD PROCESS\n");
					processId();
					exit(0);
				}//leaving child
			
				sleep(5);
				printf("\n     PARENT PROCESS\n");
				processId();
				exit(0);
			}//leaving parent	
		}//end if 2

		else if(*argv[1] == '3'){
			printf("\nThe Parent process finishes first. \n");
			printf("The Child process finishes second. \n");
			printf("The GrandChild process finishes last. \n");

			
			if((parentId = fork()) < 0){
					printf("Fork error.\n");
			}
			
			if(parentId == 0){
				printf("\nNew Processes...");
				printf("\n     PARENT PROCESS\n");
				processId();

				//create child
				if((childId = fork()) < 0){
					printf("Fork Error...\n");
				}//
			
				//inside child	
				if(childId == 0){
					printf("\n     CHILD PROCESS\n");
					processId();
			
					//create grandchild	
					if((gchildId = fork()) < 0){
						printf("Fork Error...\n");
					}
				
					if(gchildId == 0){
						printf("\n     GRANDCHILD PROCESS\n");
						processId();
						sleep(7);
						printf("\n     GRANDCHILD PROCESS\n");
						processId();
						exit(0);	
					}//leaving grandchild process
				
					sleep(5);
					printf("\n     CHILD PROCESS\n");
					processId();
					exit(0);
	 			}//leaving child

				sleep(2);
				printf("\nAfter Sleeping...");
				printf("\n     PARENT PROCESS\n");
				processId();
				exit(0);
			}//leaving parent	
		}//end else if 3

		else{
			printf("Invalid Argument...");
		}//end else 4
	}//end else	

	sleep(9);
	exit(0);

}//end main

void processId(void){
	printf("       pid: %d\n", getpid());
	printf("       ppid: %d\n", getppid());
	printf("       gid: %d\n", getgid());
	printf("       sid: %d\n", getsid(getpid()));
}//end of processId
	












