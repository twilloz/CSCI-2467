Tracey Willoz
homework1
CSCI-2467
Feburary 3, 2016

To Compile:
gcc -I../apue.3e/include -L../apue.3e/lib myls.c -lapue

To Run:
./a.out

To Test:
./a.out					//one argument      
./a.out .				//two arguments     
./a.out ..									
./a.out dirp->d_name						//empty dir -system error- cant open
./a.out -a 									
./a.out . -a 			//three arguments	
./a.out .. -a 								
./a.out dirp->d_name -a 					
./a.out -f 				//invalid flag 		
					